Source: codicefiscale
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Elena Grandi <valhalla-d@trueelena.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
Standards-Version: 3.9.8
Homepage: https://github.com/ema/pycodicefiscale
Vcs-Git: https://salsa.debian.org/python-team/packages/codicefiscale.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/codicefiscale

Package: python3-codicefiscale
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Generate and validate Italian "codice fiscale" (Python 3.x)
 This Python library works with Italian fiscal codes for natural persons
 (the local equivalent to SSN in the USA) and allows one to:
  * generate a realistic code from given data;
  * calculate the control code;
  * extract data from an existing code: birthday and sex.
 .
 This package provides Python 3.x version of codicefiscale.
